#!/bin/bash

#Get servers list
set -f
string=$AWS_IP
array=(${string//,/ })

for i in "${!array[@]}"; do
  echo "Deploying information to EC2 and Gitlab"
  echo "Deploy project on server ${array[i]}"
  ssh ubuntu@${array[i]}  "cd readme && git pull origin master && sudo docker build  -t shreeda . && sudo docker run  -d -p 80:3000 shreeda"
done 